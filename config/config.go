package config

import (
	"github.com/spf13/viper"
)

// Config contains the application configuration settings.
type Config struct {
	// App Environment
	Environment string `mapstructure:"ENV"`
	// App Port
	Port string `mapstructure:"APP_PORT"`
	// Database
	DatabaseType     string `mapstructure:"DB_TYPE"`
	DatabaseHost     string `mapstructure:"DB_HOST"`
	DatabasePort     string `mapstructure:"DB_PORT"`
	DatabaseUsername string `mapstructure:"DB_USER"`
	DatabasePassword string `mapstructure:"DB_PASS"`
	DatabaseName     string `mapstructure:"DB_NAME"`
	Debug            string `mapstructure:"DEBUG"`
	// Cache
	CacheMode string `mapstructure:"CACHE_MODE"`
	CacheTime int    `mapstructure:"CACHE_TIME"`
	// Redis
	RedisAddr string `mapstructure:"REDIS_ADDR"`
	RedisPass string `mapstructure:"REDIS_PASS"`
	RedisDB   int    `mapstructure:"REDIS_DB"`
	// Opentelemetry
	OtelStart    int `mapstructure:"OTEL_START"`
	OtelInsecure int `mapstructure:"OTEL_INSECURE"`
}

var Conf Config

// LoadConfig initializes and loads the application configuration.
func LoadConfig(path string) (conf Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigType("env")
	viper.SetConfigName(".env")

	viper.AutomaticEnv()

	//handle null
	if err := viper.ReadInConfig(); err != nil {
		return conf, err
	}
	if err := viper.Unmarshal(&conf); err != nil {
		return conf, err
	}
	Conf = conf

	return conf, err
}
