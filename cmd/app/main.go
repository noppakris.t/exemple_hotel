package main

import (
	"encoding/json"
	"fmt"

	//"go-code/api/middlewares/caches"
	"go-code/api/middlewares/caches"
	"go-code/api/routes"
	"go-code/config"
	"go-code/database"
	"go-code/pkg/repositories"
	"net/http"
	"regexp"
	"time"

	"github.com/casbin/casbin/v2"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

// Email
func IsEmail(fl validator.FieldLevel) bool {
	if fl.Field().String() == "" {
		return true
	}

	emailRegex := regexp.MustCompile(`^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$`)
	return emailRegex.MatchString(fl.Field().String())
}

// CustomValidator creates a new instance of the validator.
func CustomValidator() *validator.Validate {
	validate := validator.New()
	validate.RegisterValidation("email", IsEmail)
	return validate
}

func main() {
	conf, err := config.LoadConfig(".")
	if err != nil {
		// Handle the error, e.g., log and exit
		panic(err)
	}

	// Initialize the database
	db, err := database.Initialize(database.Config{
		DatabaseType:     conf.DatabaseType,
		DatabaseUsername: conf.DatabaseUsername,
		DatabasePassword: conf.DatabasePassword,
		DatabaseHost:     conf.DatabaseHost,
		DatabasePort:     conf.DatabasePort,
		DatabaseName:     conf.DatabaseName,
		Debug:            conf.Debug,
	})
	if err != nil {
		// Handle the error, e.g., log and exit
		panic(err)
	}
	defer database.Close(db) // Ensure the database is closed when the application exits

	// Create a Fiber app
	app := fiber.New()

	// Initialize Elastic APM
	//  apm := apmoptel.Init(&apmoptel.Config{
	// Start: conf.OtelStart,
	//  })

	app.Use(cors.New())
	app.Use(recover.New())
	app.Use(requestid.New())
	// Initialize Elastic APM middleware
	//app.Use(apm.Middleware())
	// Custom logger middleware to format logs as JSON
	app.Use(func(c *fiber.Ctx) error {
		start := time.Now()
		if err := c.Next(); err != nil {
			return err
		}
		end := time.Now()
		latency := end.Sub(start)

		// Create a map to store log data
		logData := map[string]interface{}{
			"time":       end.Format("2006-01-02T15:04:05.999999-07:00"),
			"latency":    latency.String(),
			"status":     c.Response().StatusCode(),
			"method":     c.Method(),
			"path":       c.Path(),
			"requestId":  c.Locals("requestid"),
			"reqHeaders": string(c.Request().Header.Header()),
			"reqBody":    string(c.Body()),
			"resBody":    string(c.Response().Body()),
		}

		// Convert the map to JSON
		logJSON, err := json.Marshal(logData)
		if err != nil {
			fmt.Println("Error marshaling log data to JSON:", err)
		}

		// Output the JSON log
		fmt.Printf("\n%s", string(logJSON))
		return nil
	})
	// Cache
	app.Use(caches.New(caches.Config{
		CacheMode: conf.CacheMode,
		CacheTime: conf.CacheTime,
		Address:   conf.RedisAddr,
		Password:  conf.RedisPass,
		DB:        conf.RedisDB,
	}))
	// Validator
	app.Use(func(c *fiber.Ctx) error {
		c.Locals("validator", CustomValidator())
		return c.Next()
	})
	// Casbin
	app.Use(func(c *fiber.Ctx) error {
		// init
		path := c.Path()
		method := c.Method()

		// Casbin
		cb, err := casbin.NewEnforcer("./casbin_model.conf", "./casbin_policy.csv")
		if err != nil {
			panic(err)
		}

		role := "guest"
		res, _ := cb.Enforce(role, path, method)
		if !res {
			return c.Status(http.StatusForbidden).JSON(map[string]interface{}{
				"message": "Access denied",
			})
		}

		return c.Next()
	})
	// Repository
	app.Use(func(c *fiber.Ctx) error {
		repo := repositories.New(
			repositories.WithGorm(db),
		)
		c.Locals("repository", repo)

		return c.Next()
	})

	// Route
	routes.SetupRoutes(app)

	// Start the Fiber server
	port := ":3000"
	if config.Conf.Port != "" {
		port = fmt.Sprintf(":%s", config.Conf.Port)
	}
	if err := app.Listen(port); err != nil {
		// Handle the error, e.g., log and exit
		panic(err)
	}
}
