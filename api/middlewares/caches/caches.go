package caches

import (
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cache"
)

type Config struct {
	CacheMode string // inmem, redis, none
	CacheTime int    // Seconds
	Address   string
	Password  string
	DB        int
}

func newRedisClient(conf Config) *redis.Client {
	if conf.Address == "" {
		conf.Address = "localhost:6379"
	}

	client := redis.NewClient(&redis.Options{
		Addr:     conf.Address,  // Redis server address
		Password: conf.Password, // No password by default
		DB:       conf.DB,       // Default database
	})
	return client
}

// New creates a new middleware handler
func New(conf Config) fiber.Handler {
	config := cache.Config{
		Expiration: time.Second * time.Duration(conf.CacheTime),
		KeyGenerator: func(c *fiber.Ctx) string {
			return c.OriginalURL()
		},
		CacheControl: true,
		Methods:      []string{fiber.MethodGet, fiber.MethodHead},
	}

	if conf.CacheMode == "inmem" {
		return cache.New(config)
	}

	if conf.CacheMode == "redis" {
		client := newRedisClient(conf)

		// Redis config
		config.Storage = &CacheStorage{client: client}
		return cache.New(config)
	}

	return func(c *fiber.Ctx) error {
		c.Next()
		return nil
	}
}
