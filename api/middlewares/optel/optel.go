package optel

import (
	"context"
	"fmt"
	"log"
	"os"

	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"

	"github.com/gofiber/fiber/v2"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
)

type Config struct {
	Start int
}

var Tracer = otel.Tracer("fiber-server")

func Init(ctx context.Context, conf *Config) *sdktrace.TracerProvider {
	if !(conf != nil && conf.Start == 1) {
		return nil
	}

	url := os.Getenv("ELASTIC_APM_SERVER_URL")
	token := os.Getenv("ELASTIC_APM_SECRET_TOKEN")
	svname := os.Getenv("ELASTIC_APM_SERVICE_NAME")
	if svname == "" {
		svname = "go-fiber"
	}
	insecure := os.Getenv("OTEL_INSECURE")

	// exporter, err := stdout.New(stdout.WithPrettyPrint())
	// exporter, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint("http://localhost:14268/api/traces")))
	options := []otlptracegrpc.Option{}
	if url != "" {
		options = append(options, otlptracegrpc.WithEndpoint(url)) // Default localhost:4318, 4317 (grpc)
	}
	if insecure == "1" {
		options = append(options, otlptracegrpc.WithInsecure()) // Default https (insecure = http)
	}
	if token != "" {
		options = append(options, otlptracegrpc.WithHeaders(map[string]string{"Authorization": fmt.Sprintf("Bearer %s", token)}))
	}

	exporter, err := otlptracegrpc.New(
		ctx,
		options...,
	)
	if err != nil {
		log.Fatal(err)
	}

	tp := sdktrace.NewTracerProvider(
		sdktrace.WithBatcher(exporter),
		sdktrace.WithResource(
			resource.NewWithAttributes(
				semconv.SchemaURL,
				semconv.ServiceNameKey.String(svname),
			),
		),
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
	)
	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))
	return tp
}

// Middleware
func Middleware() fiber.Handler {
	return func(c *fiber.Ctx) error {
		// Otel
		nctx, span := Tracer.Start(c.Context(), c.Path())
		defer span.End()

		// Manually set the OpenTelemetry context in Fiber context
		c.Locals("otel-context", nctx)

		// Set attributes for the span
		span.SetAttributes(
			attribute.String("http.client_ip", c.IP()),
			attribute.String("http.flavor", c.Protocol()),
			attribute.String("http.method", c.Method()),
			attribute.Int("http.request_content_length", int(c.Request().Header.ContentLength())),
			attribute.Int("http.response_content_length", c.Response().Header.ContentLength()),
			attribute.String("http.route", c.Path()),
			attribute.String("http.scheme", c.Protocol()),
			attribute.Int("http.status_code", c.Response().StatusCode()),
			attribute.String("http.target", c.OriginalURL()),
			attribute.String("http.url", c.OriginalURL()),
			attribute.String("http.user_agent", c.Get("User-Agent")),
			attribute.String("net.host.name", c.Hostname()),
			attribute.String("net.transport", "ip_tcp"),
		)
		return c.Next()
	}
}
