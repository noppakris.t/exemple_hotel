package apmoptel

import (
	"context"

	"github.com/gofiber/fiber/v2"
	"go.elastic.co/apm/module/apmotel/v2"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
)

type Config struct {
	Start int
}

type ApmOptelInterface interface {
	Middleware() fiber.Handler
	Trace(spanName string, opts ...trace.SpanStartOption) (context.Context, trace.Span)
}

type ApmOptel struct {
	Config   *Config
	Context  context.Context
	Provider trace.TracerProvider
	Tracer   trace.Tracer
}

var Optel ApmOptelInterface

func Init(conf *Config) ApmOptelInterface {
	if !(conf != nil && conf.Start == 1) {
		return nil
	}

	tp, err := apmotel.NewTracerProvider()
	if err != nil {
		return nil
	}

	Tracer := otel.Tracer("fiber-server")

	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	Optel = &ApmOptel{
		Config:   conf,
		Context:  context.Background(),
		Provider: tp,
		Tracer:   Tracer,
	}
	return Optel
}

// Middleware
func (s *ApmOptel) Middleware() fiber.Handler {
	return func(c *fiber.Ctx) error {
		if !(s.Config != nil && s.Config.Start == 1) {
			return c.Next()
		}

		// Otel
		nctx, span := s.Tracer.Start(c.Context(), c.Path())
		defer span.End()

		// Manually set the OpenTelemetry context in Fiber context
		s.Context = nctx
		Optel = s

		// Set attributes for the span
		span.SetAttributes(
			attribute.String("http.client_ip", c.IP()),
			attribute.String("http.flavor", c.Protocol()),
			attribute.String("http.method", c.Method()),
			attribute.Int("http.request_content_length", int(c.Request().Header.ContentLength())),
			attribute.Int("http.response_content_length", c.Response().Header.ContentLength()),
			attribute.String("http.route", c.Path()),
			attribute.String("http.scheme", c.Protocol()),
			attribute.Int("http.status_code", c.Response().StatusCode()),
			attribute.String("http.target", c.OriginalURL()),
			attribute.String("http.url", c.OriginalURL()),
			attribute.String("http.user_agent", c.Get("User-Agent")),
			attribute.String("net.host.name", c.Hostname()),
			attribute.String("net.transport", "ip_tcp"),
		)
		return c.Next()
	}
}

// Tracer
func (s *ApmOptel) Trace(spanName string, opts ...trace.SpanStartOption) (context.Context, trace.Span) {
	if !(s.Config != nil && s.Config.Start == 1) || s.Tracer == nil || s.Context == nil {
		return s.Context, nil
	}

	return s.Tracer.Start(s.Context, spanName, opts...)
}
