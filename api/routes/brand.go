package routes

import (
	"go-code/api/handlers"

	"github.com/gofiber/fiber/v2"
)

func SetupBrandRoutes(api fiber.Router) {
	// Create a /brands route group
	handler := handlers.NewBrandHandler()
	userGroup := api.Group("/brands")

	// Define routes for user-related operations
	userGroup.Get("", handler.GetBrands)
	userGroup.Get("/:id", handler.GetBrandByID)
	userGroup.Post("", handler.Create)
}
