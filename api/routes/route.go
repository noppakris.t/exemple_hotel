package routes

import (
	"go-code/api/handlers"

	"github.com/gofiber/fiber/v2"
)

// SetupRoutes initializes all routes for the application.
func SetupRoutes(app *fiber.App) {
	api := app.Group("/api/v1/")

	// brand routes
	{
		SetupBrandRoutes(api)
	}
	handler := handlers.NewRoomHandler()
	userGroup := api.Group("/rooms")

	// Define routes for user-related operations
	userGroup.Get("", handler.GetRooms)
	// userGroup.Get("/:id", handler.GetRoomByID)
	// userGroup.Post("", handler.Create)

	// // sku routes
	// {
	// 	SetupSkuRoutes(app)
	// }
}
