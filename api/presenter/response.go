package presenter

// ResponseHttp
type ResponseHttp struct {
	Status bool        `json:"status"`
	Total  *int64      `json:"total,omitempty"`
	Data   interface{} `json:"data"`
	Error  *string     `json:"error"`
}

// SuccessResponse is the singular SuccessResponse that will be passed in the response by
// Handler
func SuccessResponse(data interface{}) ResponseHttp {
	return ResponseHttp{
		Status: true,
		Data:   data,
		Error:  nil,
	}
}

// PagingResponse is the list SuccessResponse that will be passed in the response by Handler
func PagingResponse(total int64, data interface{}) ResponseHttp {
	return ResponseHttp{
		Status: true,
		Total:  &total,
		Data:   data,
		Error:  nil,
	}
}

// ErrorResponse is the ErrorResponse that will be passed in the response by Handler
func ErrorResponse(err error) ResponseHttp {
	strerr := err.Error()
	return ResponseHttp{
		Status: false,
		Data:   map[string]string{},
		Error:  &strerr,
	}
}
