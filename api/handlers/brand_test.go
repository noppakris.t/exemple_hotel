package handlers_test

import (
	"go-code/api/handlers"
	"go-code/pkg/entities"
	"go-code/pkg/repositories"
	"go-code/pkg/repositories/brand"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestGetBrands(t *testing.T) {
	// Create a Fiber app for testing
	app := fiber.New()

	// Mock repositories
	mockbrand := brand.NewMockRepository(t)
	mockrepo := &repositories.Repository{
		Brand: mockbrand,
	}

	// Define a test route that calls the GetBrands function
	app.Get("/api/v1/brands", func(c *fiber.Ctx) error {
		// Set the mock validator in the Fiber context
		validate := validator.New()
		c.Locals("validator", validate)

		// Set the mock repository in the Fiber context
		c.Locals("repository", mockrepo)

		// Define a test route that calls the GetBrands function
		handler := handlers.NewBrandHandler()
		if err := handler.GetBrands(c); err != nil {
			return err
		}

		return nil
	})

	t.Run("valid", func(t *testing.T) {
		// Mocking the FindByID method with expected response
		data := []entities.Brand{
			{
				BrandId: uint64(1),
				ThBrand: "Th",
				EnBrand: "En",
			},
		}
		exptotal := int64(1)
		mockbrand.On("Count",
			mock.Anything,
			mock.AnythingOfType("brand.WhereSearch"),
			mock.Anything,
			mock.Anything,
		).Return(exptotal, nil)
		mockbrand.On("Find",
			mock.Anything,
			mock.AnythingOfType("brand.WhereSearch"),
			mock.Anything,
			mock.Anything,
		).Return(&data, nil)

		// Create a test request with a specific URI and query parameters
		req := httptest.NewRequest(http.MethodGet, "/api/v1/brands", nil)
		resp, err := app.Test(req)

		// Asserting the result
		assert.Nil(t, err)
		assert.Equal(t, http.StatusOK, resp.StatusCode)
	})
}
