package handlers

import (
	"errors"
	"go-code/api/presenter"
	"go-code/pkg/repositories"
	"go-code/pkg/services"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

type RoomInterface interface {
	GetRooms(c *fiber.Ctx) error
}

type RoomHandler struct {
}

// NewRoomHandler creates a new RoomHandler instance with the provided RoomRepository.
func NewRoomHandler() RoomInterface {
	return &RoomHandler{}
}

// GetRooms is a function to get all brands data from the database.
// @Summary Get all brands
// @Description Get all brands based on query parameters.
// @Tags brands
// @ID get-brands
// @Accept json
// @Produce json
// @Param query query string false "Search query"
// @Param page query int false "Page number"
// @Param limit query int false "Number of results per page"
// @Param order query string false "Sorting order"
// @Success 200 {object} presenter.ResponseHttp{data=[]entities.Room}
// @Failure 400 {object} presenter.ResponseHttp
// @Failure 500 {object} presenter.ResponseHttp
// @Router /brands [get]
func (h *RoomHandler) GetRooms(c *fiber.Ctx) error {
	// Repository
	repo, err := repositories.GetRepositoryFromContext(c)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(err),
		)
	}

	// Get the custom validator instance from locals
	v, ok := c.Locals("validator").(*validator.Validate)
	if !ok {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("validator not found")),
		)
	}

	// Create an instance
	req := services.OptionFindRoom{}

	// Parse and bind the JSON request body to the struct
	if err := c.QueryParser(&req); err != nil {
		return c.Status(http.StatusBadRequest).JSON(
			presenter.ErrorResponse(errors.New("invalid json data")),
		)
	}

	// Perform validation
	if err := v.Struct(req); err != nil {
		return c.Status(http.StatusBadRequest).JSON(
			presenter.ErrorResponse(errors.New("validation failed")),
		)
	}

	// Find
	sv := services.NewRoom(repo)
	total, resp, err := sv.Find(c.Context(), &req)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("failed to retrieve data")),
		)
	}

	// Return the data as JSON response
	return c.JSON(presenter.PagingResponse(total, resp))
}
