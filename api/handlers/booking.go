package handlers

import (
	"errors"
	"go-code/api/presenter"
	"go-code/pkg/repositories"
	"go-code/pkg/services"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

type BookingInterface interface {
	Create(c *fiber.Ctx) error
}

type BookingHandler struct {
}

// NewBookingHandler creates a new BookingHandler instance with the provided BookingRepository.
func NewBookingHandler() BookingInterface {
	return &BookingHandler{}
}

// Create handles requests to create a new brand.
// @Summary Create a brand
// @Description Create a new brand.
// @Tags brands
// @ID create-brand
// @Accept json
// @Produce json
// @Param brandData body services.ReqCreateBooking true "Booking data"
// @Success 201 {object} presenter.ResponseHttp{data=entities.Booking}
// @Failure 400 {object} presenter.ResponseHttp
// @Failure 500 {object} presenter.ResponseHttp
// @Router /brands [post]

func (h *BookingHandler) Create(c *fiber.Ctx) error {
	// Repository
	repo, err := repositories.GetRepositoryFromContext(c)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(err),
		)
	}

	// Get the custom validator instance from locals
	v, ok := c.Locals("validator").(*validator.Validate)
	if !ok {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("validator not found")),
		)
	}

	// Create an instance
	req := services.ReqCreateBooking{}

	// Parse and bind the JSON request body to the struct
	if err := c.BodyParser(&req); err != nil {
		return c.Status(http.StatusBadRequest).JSON(
			presenter.ErrorResponse(errors.New("invalid json data")),
		)
	}

	// Perform validation
	if err := v.Struct(req); err != nil {
		return c.Status(http.StatusBadRequest).JSON(
			presenter.ErrorResponse(errors.New("validation failed")),
		)
	}

	tx := repo.Booking.BeginTransaction()
	isComplete := false
	defer func() {
		if isComplete {
			repositories.Commit(tx)
		} else {
			repositories.Rollback(tx)
		}
	}()

	brandrepo, err := repo.Booking.New(tx)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("failed to start transaction")),
		)
	}
	repo.Booking = brandrepo

	// Create
	sv := services.NewBooking(repo)
	resp, err := sv.Create(c.Context(), &req)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("failed to create data")),
		)
	}

	isComplete = true
	// Return the data as JSON response
	return c.JSON(presenter.SuccessResponse(resp))
}
