package handlers

import (
	"errors"
	"fmt"
	"go-code/api/middlewares/apmoptel"
	"go-code/api/presenter"
	"go-code/pkg/entities"
	"go-code/pkg/repositories"
	"go-code/pkg/services"
	"net/http"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

type BrandInterface interface {
	GetBrands(c *fiber.Ctx) error
	GetBrandByID(c *fiber.Ctx) error
	Create(c *fiber.Ctx) error
}

type BrandHandler struct {
}

// NewBrandHandler creates a new BrandHandler instance with the provided BrandRepository.
func NewBrandHandler() BrandInterface {
	return &BrandHandler{}
}

// GetBrands is a function to get all brands data from the database.
// @Summary Get all brands
// @Description Get all brands based on query parameters.
// @Tags brands
// @ID get-brands
// @Accept json
// @Produce json
// @Param query query string false "Search query"
// @Param page query int false "Page number"
// @Param limit query int false "Number of results per page"
// @Param order query string false "Sorting order"
// @Success 200 {object} presenter.ResponseHttp{data=[]entities.Brand}
// @Failure 400 {object} presenter.ResponseHttp
// @Failure 500 {object} presenter.ResponseHttp
// @Router /brands [get]
func (h *BrandHandler) GetBrands(c *fiber.Ctx) error {
	// Repository
	repo, err := repositories.GetRepositoryFromContext(c)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(err),
		)
	}

	// Get the custom validator instance from locals
	v, ok := c.Locals("validator").(*validator.Validate)
	if !ok {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("validator not found")),
		)
	}

	// Create an instance
	req := services.OptionFindBrand{}

	// Parse and bind the JSON request body to the struct
	if err := c.QueryParser(&req); err != nil {
		return c.Status(http.StatusBadRequest).JSON(
			presenter.ErrorResponse(errors.New("invalid json data")),
		)
	}

	// Perform validation
	if err := v.Struct(req); err != nil {
		return c.Status(http.StatusBadRequest).JSON(
			presenter.ErrorResponse(errors.New("validation failed")),
		)
	}

	// Find
	sv := services.NewBrand(repo)
	total, resp, err := sv.Find(c.Context(), &req)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("failed to retrieve data")),
		)
	}

	// Return the data as JSON response
	return c.JSON(presenter.PagingResponse(total, resp))
}

// GetBrandByID handles requests to get a brand by their ID.
// @Summary Get a brand by ID
// @Description Get a brand by their ID.
// @Tags brands
// @ID get-brand-by-id
// @Produce json
// @Param id path int true "User ID"
// @Success 200 {object} presenter.ResponseHttp{data=entities.Brand}
// @Failure 400 {object} presenter.ResponseHttp
// @Failure 404 {object} presenter.ResponseHttp
// @Failure 500 {object} presenter.ResponseHttp
// @Router /brands/{id} [get]
func (h *BrandHandler) GetBrandByID(c *fiber.Ctx) error {
	// Repository
	repo, err := repositories.GetRepositoryFromContext(c)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(err),
		)
	}

	// Parse the ID from the request parameters
	id, err := strconv.ParseUint(c.Params("id"), 10, 64)
	if err != nil {
		return c.Status(http.StatusBadRequest).JSON(map[string]interface{}{
			"error": "Invalid id",
		})
	}
	ctx := c.Context()

	// Tracer
	if apmoptel.Optel != nil {
		_, span := apmoptel.Optel.Trace("handlers.GetBrandByID", trace.WithAttributes(attribute.String("id", fmt.Sprintf("%d", id))))
		if span != nil {
			defer span.End()
		}
	}

	sv := services.NewBrand(repo)
	resp, err := sv.FindByID(ctx, id)
	if err != nil {
		if err == entities.ErrBrandNotFound {
			return c.Status(http.StatusNotFound).JSON(
				presenter.ErrorResponse(errors.New("data not found")),
			)
		}
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("failed to retrieve data")),
		)
	}

	// Return the data as JSON response
	return c.JSON(presenter.SuccessResponse(resp))
}

// Create handles requests to create a new brand.
// @Summary Create a brand
// @Description Create a new brand.
// @Tags brands
// @ID create-brand
// @Accept json
// @Produce json
// @Param brandData body services.ReqCreateBrand true "Brand data"
// @Success 201 {object} presenter.ResponseHttp{data=entities.Brand}
// @Failure 400 {object} presenter.ResponseHttp
// @Failure 500 {object} presenter.ResponseHttp
// @Router /brands [post]
func (h *BrandHandler) Create(c *fiber.Ctx) error {
	// Repository
	repo, err := repositories.GetRepositoryFromContext(c)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(err),
		)
	}

	// Get the custom validator instance from locals
	v, ok := c.Locals("validator").(*validator.Validate)
	if !ok {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("validator not found")),
		)
	}

	// Create an instance
	req := services.ReqCreateBrand{}

	// Parse and bind the JSON request body to the struct
	if err := c.BodyParser(&req); err != nil {
		return c.Status(http.StatusBadRequest).JSON(
			presenter.ErrorResponse(errors.New("invalid json data")),
		)
	}

	// Perform validation
	if err := v.Struct(req); err != nil {
		return c.Status(http.StatusBadRequest).JSON(
			presenter.ErrorResponse(errors.New("validation failed")),
		)
	}

	tx := repo.Brand.BeginTransaction()
	isComplete := false
	defer func() {
		if isComplete {
			repositories.Commit(tx)
		} else {
			repositories.Rollback(tx)
		}
	}()

	brandrepo, err := repo.Brand.New(tx)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("failed to start transaction")),
		)
	}
	repo.Brand = brandrepo

	// Create
	sv := services.NewBrand(repo)
	resp, err := sv.Create(c.Context(), req)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(
			presenter.ErrorResponse(errors.New("failed to create data")),
		)
	}

	isComplete = true
	// Return the data as JSON response
	return c.JSON(presenter.SuccessResponse(resp))
}
