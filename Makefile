.PHONY: build run swag test test-view

build:
	go build -o main cmd/app/main.go

run:
	go run cmd/app/main.go

swag:
	swag init -d cmd/app/ -o docs/v1 --parseDependency --pdl 2

mock:
	mockery --config=./mockery.yaml

test:
	go test -coverprofile=coverage.out ./...

test-view:
	go test -coverprofile=coverage.out ./...;
	go tool cover -html=coverage.out;