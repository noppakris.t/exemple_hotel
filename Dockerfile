FROM golang:1.21-alpine AS go-build
RUN apk update; \
  apk add git
RUN mkdir -p /app
WORKDIR /app

COPY go.mod go.sum /app/
COPY . /app/
RUN go mod download

RUN go install github.com/swaggo/swag/cmd/swag@latest
RUN swag init -d ./cmd -o ./docs/v1 --parseDependency --pdl 2

RUN go build -o /main cmd/main.go

FROM alpine:latest as alpine

RUN apk update;
RUN apk add tzdata;

COPY --from=go-build /main /application/main
WORKDIR /application

COPY ./casbin_model.conf  /application/casbin_model.conf
COPY ./casbin_policy.csv  /application/casbin_policy.csv
RUN mkdir -p /docs/v1

COPY --from=go-build /app/docs/v1/*  /application/docs/v1/

ENV TZ=Asia/Bangkok

EXPOSE 8080

CMD ["/application/main"]