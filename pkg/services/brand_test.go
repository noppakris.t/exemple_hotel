package services_test

import (
	"context"
	"go-code/pkg/entities"
	"go-code/pkg/repositories"
	"go-code/pkg/repositories/brand"
	"go-code/pkg/services"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestUserService_Find(t *testing.T) {
	// Mock
	mockrepo := brand.NewMockRepository(t)

	sv := services.NewBrand(&repositories.Repository{
		Brand: mockrepo,
	})

	t.Run("valid", func(t *testing.T) {
		// Mocking the FindByID method with expected response
		data := []entities.Brand{
			{
				BrandId: uint64(1),
				ThBrand: "Th",
				EnBrand: "En",
			},
		}
		exptotal := int64(1)
		mockrepo.On("Count",
			mock.Anything,
			mock.AnythingOfType("brand.WhereSearch"),
			mock.Anything,
			mock.Anything,
		).Return(exptotal, nil)
		mockrepo.On("Find",
			mock.Anything,
			mock.AnythingOfType("brand.WhereSearch"),
			mock.Anything,
			mock.Anything,
		).Return(&data, nil)

		// Calling the function to be tested
		total, res, err := sv.Find(context.Background(), &services.OptionFindBrand{})
		items := *res

		// Asserting the result
		assert.Nil(t, err)
		assert.Equal(t, exptotal, total)
		assert.Equal(t, len(data), len(items))
		assert.Equal(t, data[0].BrandId, items[0].BrandId)
		assert.Equal(t, data[0].ThBrand, items[0].ThBrand)
		assert.Equal(t, data[0].EnBrand, items[0].EnBrand)
	})

	mockrepo.AssertExpectations(t)
}

func TestUserService_FindByID(t *testing.T) {
	// Mock
	mockrepo := brand.NewMockRepository(t)

	sv := services.NewBrand(&repositories.Repository{
		Brand: mockrepo,
	})

	t.Run("valid", func(t *testing.T) {
		// Mocking the FindByID method with expected response
		data := entities.Brand{
			BrandId: uint64(1),
			ThBrand: "Th",
			EnBrand: "En",
		}
		mockrepo.On("FindByID", mock.Anything, mock.Anything).Return(&data, nil)

		// Calling the function to be tested
		res, err := sv.FindByID(context.Background(), data.BrandId)

		// Asserting the result
		assert.Nil(t, err)
		assert.Equal(t, data.BrandId, res.BrandId)
		assert.Equal(t, data.ThBrand, res.ThBrand)
		assert.Equal(t, data.EnBrand, res.EnBrand)
	})

	mockrepo.AssertExpectations(t)
}

func TestUserService_Create(t *testing.T) {
	// Mock
	mockrepo := brand.NewMockRepository(t)

	// Service
	sv := services.NewBrand(&repositories.Repository{
		Brand: mockrepo,
	})

	t.Run("valid", func(t *testing.T) {
		// Mocking the Create method with expected response
		data := entities.Brand{
			BrandId: uint64(1),
			ThBrand: "Th",
			EnBrand: "En",
		}
		mockrepo.On("Create", mock.Anything, mock.AnythingOfType("*entities.Brand")).Return(nil)

		// Calling the function to be tested
		res, err := sv.Create(context.Background(), services.ReqCreateBrand{
			ThBrand: data.ThBrand,
			EnBrand: data.EnBrand,
		})

		// Asserting the result
		assert.Nil(t, err)
		assert.Equal(t, data.ThBrand, res.ThBrand)
		assert.Equal(t, data.EnBrand, res.EnBrand)
	})

	mockrepo.AssertExpectations(t)
}
