package services

import (
	"context"
	"go-code/pkg/entities"
	"go-code/pkg/functions/gormquery"
	"go-code/pkg/repositories"
	"go-code/pkg/repositories/room"
)

// type ReqCreateRoom struct {
// 	ThRoom string `json:"thRoom" form:"thRoom"`
// 	EnRoom string `json:"enRoom" form:"enRoom"`
// }

type OptionFindRoom struct {
	Query string `query:"query"`
	Page  int    `query:"page"`
	Limit int    `query:"limit"`
	Order string `query:"order"`
}

type RoomInterface interface {
	Find(ctx context.Context, option *OptionFindRoom) (total int64, data *[]entities.Room, err error)
}

// Service implement service interface
type RoomService struct {
	Repo *repositories.Repository
}

// NewRoom
func NewRoom(repo *repositories.Repository) RoomInterface {
	return &RoomService{
		Repo: repo,
	}
}

// Find
func (s *RoomService) Find(ctx context.Context, option *OptionFindRoom) (total int64, data *[]entities.Room, err error) {
	cond := []gormquery.Query{}
	if option != nil {
		cond = append(
			cond,
			room.WhereSearch{Query: option.Query},
		)
	}

	total, err = s.Repo.Room.Count(ctx, cond...)
	if err != nil {
		return total, data, err
	}

	cond = append(
		cond,
		gormquery.LimitPage{Page: option.Page, Limit: option.Limit},
		gormquery.OrderBy{Order: &option.Order, Repo: s.Repo.Room},
	)
	data, err = s.Repo.Room.Find(ctx, cond...)
	return total, data, err
}
