package services

import (
	"context"
	"fmt"
	"go-code/api/middlewares/apmoptel"
	"go-code/pkg/entities"
	"go-code/pkg/functions/gormquery"
	"go-code/pkg/repositories"
	"go-code/pkg/repositories/brand"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

type ReqCreateBrand struct {
	ThBrand string `json:"thBrand" form:"thBrand"`
	EnBrand string `json:"enBrand" form:"enBrand"`
}

type OptionFindBrand struct {
	Query string `query:"query"`
	Page  int    `query:"page"`
	Limit int    `query:"limit"`
	Order string `query:"order"`
}

type BrandInterface interface {
	Find(ctx context.Context, option *OptionFindBrand) (total int64, data *[]entities.Brand, err error)
	FindByID(ctx context.Context, id uint64) (data *entities.Brand, err error)
	Create(ctx context.Context, req ReqCreateBrand) (data *entities.Brand, err error)
}

// Service implement service interface
type BrandService struct {
	Repo *repositories.Repository
}

// NewBrand
func NewBrand(repo *repositories.Repository) BrandInterface {
	return &BrandService{
		Repo: repo,
	}
}

// Find
func (s *BrandService) Find(ctx context.Context, option *OptionFindBrand) (total int64, data *[]entities.Brand, err error) {
	cond := []gormquery.Query{}
	if option != nil {
		cond = append(
			cond,
			brand.WhereSearch{Query: option.Query},
		)
	}

	total, err = s.Repo.Brand.Count(ctx, cond...)
	if err != nil {
		return total, data, err
	}

	cond = append(
		cond,
		gormquery.LimitPage{Page: option.Page, Limit: option.Limit},
		gormquery.OrderBy{Order: &option.Order, Repo: s.Repo.Brand},
	)
	data, err = s.Repo.Brand.Find(ctx, cond...)
	return total, data, err
}

// FindByID
func (s *BrandService) FindByID(ctx context.Context, id uint64) (data *entities.Brand, err error) {
	// Tracer
	if apmoptel.Optel != nil {
		_, span := apmoptel.Optel.Trace("services.FindByID", trace.WithAttributes(attribute.String("id", fmt.Sprintf("%d", id))))
		if span != nil {
			defer span.End()
		}
	}

	return s.Repo.Brand.FindByID(ctx, id)
}

// Create
func (s *BrandService) Create(ctx context.Context, req ReqCreateBrand) (data *entities.Brand, err error) {
	data = &entities.Brand{}
	data.ThBrand = req.ThBrand
	data.EnBrand = req.EnBrand
	if s.Repo.Brand.Create(ctx, data); err != nil {
		return data, err
	}

	return data, err
}
