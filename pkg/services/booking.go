package services

import (
	"context"
	"go-code/pkg/entities"
	"go-code/pkg/repositories"
)

// type ReqCreateBooking struct {
// 	ThBooking string `json:"thBooking" form:"thBooking"`
// 	EnBooking string `json:"enBooking" form:"enBooking"`
// }

type ReqCreateBooking struct {
	Status string `json:"status" form:"status"`
}

type BookingInterface interface {
	Create(ctx context.Context, req *ReqCreateBooking) (data *entities.Booking, err error)
}

// Service implement service interface
type BookingService struct {
	Repo *repositories.Repository
}

// NewBooking
func NewBooking(repo *repositories.Repository) BookingInterface {
	return &BookingService{
		Repo: repo,
	}
}

// Create
func (s *BookingService) Create(ctx context.Context, req *ReqCreateBooking) (data *entities.Booking, err error) {
	data = &entities.Booking{}
	data.Status = entities.Status(req.Status)
	if s.Repo.Booking.Create(ctx, data); err != nil {
		return data, err
	}

	return data, err
}
