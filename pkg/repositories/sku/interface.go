package sku

import (
	"context"
	"go-code/pkg/entities"

	"gorm.io/gorm"
)

type Repository interface {
	New(db interface{}) (Repository, error)
	BeginTransaction() *gorm.DB
	FindByID(ctx context.Context, id uint64) (*entities.Sku, error)
	Create(ctx context.Context, data *entities.Sku) error
}
