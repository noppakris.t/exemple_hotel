package brand

import (
	"context"
	"go-code/pkg/entities"
	"go-code/pkg/functions/gormquery"

	"gorm.io/gorm"
)

type Repository interface {
	gormquery.OrderInterface
	New(db interface{}) (Repository, error)
	BeginTransaction() *gorm.DB
	Count(ctx context.Context, queries ...gormquery.Query) (int64, error)
	Find(ctx context.Context, queries ...gormquery.Query) (*[]entities.Brand, error)
	FindByID(ctx context.Context, id uint64) (*entities.Brand, error)
	Create(ctx context.Context, data *entities.Brand) error
	Update(ctx context.Context, data *entities.Brand) error
	DeleteByID(ctx context.Context, id uint64) error
	Delete(ctx context.Context, queries ...gormquery.Query) error
}
