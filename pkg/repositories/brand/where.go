package brand

import (
	"go-code/pkg/functions/gormquery"

	"gorm.io/gorm"
)

// Where
type WhereTh struct {
	ThBrand string
}

// DB implement interface
func (f WhereTh) DB(db gormquery.DB) *gorm.DB {
	g := db.(*gorm.DB)
	if f.ThBrand == "" {
		return g
	}
	return g.Where("brands.th_brand=?", f.ThBrand)
}

// Where
type WhereSearch struct {
	Query string
}

// DB implement interface
func (f WhereSearch) DB(db gormquery.DB) *gorm.DB {
	g := db.(*gorm.DB)
	if f.Query != "" {
		query := "%" + f.Query + "%"
		return g.Where("brands.th_brand LIKE ? OR brands.en_brand LIKE ?", query, query)
	}
	return g
}
