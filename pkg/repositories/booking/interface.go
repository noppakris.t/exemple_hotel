package booking

import (
	"context"
	"go-code/pkg/entities"
	"go-code/pkg/functions/gormquery"

	"gorm.io/gorm"
)

type Repository interface {
	gormquery.OrderInterface
	New(db interface{}) (Repository, error)
	BeginTransaction() *gorm.DB
	Count(ctx context.Context, queries ...gormquery.Query) (int64, error)
	Find(ctx context.Context, queries ...gormquery.Query) (*[]entities.Booking, error)
	FindByID(ctx context.Context, id uint64) (*entities.Booking, error)
	Create(ctx context.Context, data *entities.Booking) error
	Update(ctx context.Context, data *entities.Booking) error
	DeleteByID(ctx context.Context, id uint64) error
	Delete(ctx context.Context, queries ...gormquery.Query) error
}
