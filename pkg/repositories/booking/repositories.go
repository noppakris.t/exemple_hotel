package booking

import (
	"context"
	"fmt"
	"go-code/api/middlewares/apmoptel"
	"go-code/pkg/entities"
	"go-code/pkg/functions/gormquery"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"gorm.io/gorm"
)

type Gorm struct {
	db *gorm.DB
}

func New(db *gorm.DB) Repository {
	return &Gorm{db}
}

// New with argument
func (g *Gorm) New(db interface{}) (Repository, error) {
	if db == nil {
		return nil, fmt.Errorf("cannot handle more than 1 argument")
	}

	ndb, ok := db.(*gorm.DB)
	if !ok {
		return g, nil
	}

	return New(ndb), nil
}

func (r *Gorm) BeginTransaction() *gorm.DB {
	return r.db.Begin()
}

func (r *Gorm) Count(ctx context.Context, queries ...gormquery.Query) (cnt int64, err error) {
	db := gormquery.Concat(r.db, queries...)
	if err := db.Model(&entities.Booking{}).Count(&cnt).Error; err != nil {
		return cnt, err
	}
	return cnt, nil
}

func (r *Gorm) Find(ctx context.Context, queries ...gormquery.Query) (datas *[]entities.Booking, err error) {
	db := gormquery.Concat(r.db, queries...)
	datas = &[]entities.Booking{}
	if err := db.Find(&datas).Error; err != nil {
		return datas, err
	}
	return datas, nil
}

func (r *Gorm) FindByID(ctx context.Context, id uint64) (data *entities.Booking, err error) {
	_, span := apmoptel.Optel.Trace("repository.FindByID", trace.WithAttributes(attribute.String("id", fmt.Sprintf("%d", id))))
	if span != nil {
		defer span.End()
	}

	data = &entities.Booking{}
	if err := r.db.First(data, id).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return data, entities.ErrBookingNotFound
		}
		return data, err
	}
	return data, nil
}

func (r *Gorm) Create(ctx context.Context, data *entities.Booking) (err error) {
	if err := r.db.Create(data).Error; err != nil {
		return err
	}

	return nil
}

func (r *Gorm) Update(ctx context.Context, data *entities.Booking) (err error) {
	if err := r.db.Save(data).Error; err != nil {
		return err
	}

	return nil
}

func (r *Gorm) DeleteByID(ctx context.Context, id uint64) (err error) {
	data := entities.Booking{}
	if err := r.db.First(&data, "id = ?", id).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return entities.ErrBookingNotFound
		}
		return err
	}
	if err := r.db.Delete(&data).Error; err != nil {
		return err
	}

	return nil
}

// Deletes delete token
func (r *Gorm) Delete(ctx context.Context, queries ...gormquery.Query) (err error) {
	db := gormquery.Concat(r.db, queries...)
	if err := db.Delete(&entities.Booking{}).Error; err != nil {
		return err
	}
	return nil
}

// TranslateOrderField translate order
func (g *Gorm) TranslateOrderField(input string) (output string) {
	list := map[string]string{
		"id":          "bookings.id",
		"roomId":      "booking.room_id",
		"userId":      "booking.user_id",
		"status":      "booking.status",
		"bookingDate": "booking.booking_date",
		"createdAt":   "rooms.created_at",
		"updatedAt":   "rooms.updated_at",
	}

	output, ok := list[input]
	if !ok {
		return ""
	}
	return output
}
