package room

import (
	"go-code/pkg/functions/gormquery"

	"gorm.io/gorm"
)

// Where
type WhereSearch struct {
	Query string
}

// DB implement interface
func (f WhereSearch) DB(db gormquery.DB) *gorm.DB {
	g := db.(*gorm.DB)
	if f.Query != "" {
		query := "%" + f.Query + "%"
		return g.Where("rooms.room_number LIKE ? OR rooms.floor LIKE ? OR rooms.room_type LIKE ?", query, query, query)
	}
	return g
}
