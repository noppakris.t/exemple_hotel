package repositories

import (
	"errors"
	"go-code/pkg/repositories/booking"
	"go-code/pkg/repositories/brand"
	"go-code/pkg/repositories/room"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type Repository struct {
	Brand   brand.Repository
	Room    room.Repository
	Booking booking.Repository
}

type Configuration func(r *Repository)

func New(cfgs ...Configuration) *Repository {
	r := &Repository{}
	for _, cfg := range cfgs {
		cfg(r)
	}
	return r
}

func WithGorm(db *gorm.DB) Configuration {
	return func(r *Repository) {
		r.Brand = brand.New(db)
		r.Room = room.New(db)
		r.Booking = booking.New(db)
	}
}

func GetRepositoryFromContext(c *fiber.Ctx) (*Repository, error) {
	// Retrieve the "Repo" value from the context
	repoFromContext := c.Locals("repository")

	// Check if the "Repo" value exists in the context
	if repoFromContext == nil {
		return nil, errors.New("[repository] not found in context")
	}

	// Type-assert the retrieved value to the appropriate type
	repo, ok := repoFromContext.(*Repository)
	if !ok {
		return nil, errors.New("[repository] has an unexpected type")
	}

	return repo, nil
}

func Commit(db *gorm.DB) {
	db.Commit()
}

func Rollback(db *gorm.DB) {
	db.Rollback()
}
