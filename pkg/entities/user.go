package entities

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

var (
	ErrUserNotFound = errors.New("user not found")
)

type User struct {
	ID        uint64         `gorm:"primaryKey" json:"id" example:"1"`
	FirstName string         `json:"firstName" example:"Th"`
	LastName  string         `json:"lastName" example:"En"`
	Phone     string         `json:"phone" example:"En"`
	Email     string         `json:"email" example:"En"`
	CreatedAt time.Time      `json:"createdAt" example:"2023-09-27T10:56:50.089+07:00"`
	UpdatedAt time.Time      `json:"updatedAt" example:"2023-09-27T10:56:50.089+07:00"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
}
