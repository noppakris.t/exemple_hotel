package entities

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

type RoomType string

const (
	Standard RoomType = "standard"
	Deluxe   RoomType = "deluxe"
	Superior RoomType = "superior"
	Suite    RoomType = "suite"
)

var (
	ErrRoomNotFound = errors.New("room not found")
)

type Room struct {
	ID         uint64         `gorm:"primaryKey" json:"roomId" example:"1"`
	RoomNumber string         `json:"roomNumber" example:"En"`
	Floor      string         `json:"floor" example:"En"`
	Available  bool           `json:"available" example:"1"`
	RoomType   RoomType       `gorm:"type:enum('standard','deluxe','superior','suite')" json:"roomType" example:"En"`
	CreatedAt  time.Time      `json:"createdAt" example:"2023-09-27T10:56:50.089+07:00"`
	UpdatedAt  time.Time      `json:"updatedAt" example:"2023-09-27T10:56:50.089+07:00"`
	DeletedAt  gorm.DeletedAt `gorm:"index" json:"-"`
}
