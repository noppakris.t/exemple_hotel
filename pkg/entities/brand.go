package entities

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

var (
	ErrBrandNotFound = errors.New("brand not found")
)

type Brand struct {
	BrandId   uint64         `gorm:"primaryKey" json:"brandId" example:"1"`
	ThBrand   string         `json:"thBrand" example:"Th"`
	EnBrand   string         `json:"enBrand" example:"En"`
	CreatedAt time.Time      `json:"createdAt" example:"2023-09-27T10:56:50.089+07:00"`
	UpdatedAt time.Time      `json:"updatedAt" example:"2023-09-27T10:56:50.089+07:00"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
}
