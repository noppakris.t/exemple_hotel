package entities

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

type Status string

const (
	Available Status = "available"
	Reserved  Status = "reserved"
	Cancle    Status = "cancle"
)

var (
	ErrBookingNotFound = errors.New("booking not found")
)

type Booking struct {
	ID          uint64         `gorm:"primaryKey" json:"bookingId" example:"1"`
	RoomId      uint64         `json:"roomId" example:"1"`
	UserId      uint64         `json:"userId" example:"1"`
	Status      Status         `gorm:"type:enum('available','reserved','cancle')" json:"status"`
	BookingDate time.Time      `json:"bookingDate" example:"2023-09-27T10:56:50.089+07:00"`
	CreatedAt   time.Time      `json:"createdAt" example:"2023-09-27T10:56:50.089+07:00"`
	UpdatedAt   time.Time      `json:"updatedAt" example:"2023-09-27T10:56:50.089+07:00"`
	DeletedAt   gorm.DeletedAt `gorm:"index" json:"-"`
	Room        Room           `gorm:"foreignKey:RoomId"`
	User        User           `gorm:"foreignKey:UserId"`
}
