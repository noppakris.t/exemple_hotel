package entities

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

var (
	ErrTransectionNotFound = errors.New("transection not found")
)

type Transection struct {
	ID              uint64         `gorm:"primaryKey" json:"brandId" example:"1"`
	RoomId          uint64         `json:"roomId" example:"Th"`
	UserId          uint64         `json:"userId" example:"En"`
	TransectionDate time.Time      `json:"transectionDate" example:"2023-09-27T10:56:50.089+07:00"`
	CreatedAt       time.Time      `json:"createdAt" example:"2023-09-27T10:56:50.089+07:00"`
	UpdatedAt       time.Time      `json:"updatedAt" example:"2023-09-27T10:56:50.089+07:00"`
	DeletedAt       gorm.DeletedAt `gorm:"index" json:"-"`
	Room            Room           `gorm:"foreignKey:RoomId"`
	User            User           `gorm:"foreignKey:UserId"`
}
