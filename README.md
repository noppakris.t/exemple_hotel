# Project Name

A brief description of your Go project.

## Table of Contents

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
  - [Run](#run)
  - [Swagger Documentation](#swagger-documentation)
  - [Build](#build)

## Getting Started

These instructions will help you set up and run the project on your local machine.

### Prerequisites

Before getting started, make sure you have the following prerequisites installed and configured:

- **Go (at least Go 1.21)**: Ensure that you have Go installed on your system. You can download and install the latest version of Go from the [official Go website](https://golang.org/dl/).

- **Fiber**: You can install the Fiber web framework using the following Go command:

  ```shell
  go get github.com/gofiber/fiber/v2
  ```

- **MySQL**: You'll need a MySQL database instance for this project. Make sure MySQL is installed, and you have the necessary credentials (username and password) to access the database.

- **Environment Variables**: Create a .env file in your project directory based on the provided .env.example file. Configure the environment variables as needed for your project. You might need to set database connection details, API keys, or any other sensitive information in this file.

Make sure to update the .env file with the appropriate values according to your project's requirements.

## Additional Tools

To enhance your development experience, consider installing the following tools:

### 1. Swagger (swag)

[Swagger](https://github.com/swaggo/swag) is a tool that simplifies API development by providing automated documentation generation for your Go projects.

#### Installation

```bash
go install github.com/swaggo/swag/cmd/swag@latest
```

### 2. Mockery

[Mockery](https://github.com/vektra/mockery) is a mock generation tool for Go. It helps generate mocks for your interfaces, making it easier to write unit tests.

#### Installation

```bash
go install github.com/vektra/mockery/v2@v2.38.0
```

### Getting Started

Clone the repository to your local machine:

```shell
git clone https://gitlab.com/patiphan.c/go-fiber-structure.git
```

```shell
make swag
```

Or 

```shell
swag init -d cmd/app/ -o docs/v1 --parseDependency --pdl 2
```

## Usage

### Run

You can run the project with the following command:

```shell
make run
```

This command starts the server. By default, the server will start on http://localhost:3000.

### Swagger Documentation

To generate Swagger documentation for your API, use the following command:

```shell
make swag
```

This command initializes Swagger documentation using Swag. The generated documentation will be placed in the ./docs/v1 directory.

To access the Swagger documentation for this API, http://localhost:3000/api/v1/docs

### Build

To build the project, run the following command:

```shell
make build
```

### Run mock interfaces

To run mock interface, use the following command:

```shell
make mock
```

Ensure that your mockery.yaml file in the root of your project.

This configuration specifies the package name and the output directory for the generated mock files.

### Run Tests

To run tests, use the following command:

```shell
make test
```

This will execute all tests and generate a coverage report.

### View Test Coverage

To view the test coverage in a web browser, run:

```shell
make test-view
```

This command will generate a coverage report and open it in your default web browser.