package database

import (
	"fmt"
	"io"
	"os"
	"time"

	"go-code/pkg/entities"

	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	maxRetries = 3               // Set the maximum number of retries
	retryDelay = 5 * time.Second // Set the delay between retries
)

type Config struct {
	DatabaseType     string
	DatabaseUsername string
	DatabasePassword string
	DatabaseHost     string
	DatabasePort     string
	DatabaseName     string
	Debug            string
}

// CustomWriter is a custom logger.Writer implementation that wraps an io.Writer.
type CustomWriter struct {
	writer io.Writer
}

// Printf writes the log message to the underlying io.Writer.
func (cw *CustomWriter) Printf(format string, args ...interface{}) {
	_, _ = fmt.Fprintf(cw.writer, format, args...)
}

// Initialize initializes the database connection and returns a reference to the database.
func Initialize(conf Config) (db *gorm.DB, err error) {
	// Open a database connection using GORM with retries
	for attempt := 1; attempt <= maxRetries; attempt++ {
		db, err = gorm.Open(dsn(conf), &gorm.Config{
			SkipDefaultTransaction: true,
			Logger: logger.New(
				&CustomWriter{writer: os.Stdout}, // io writer
				logger.Config{
					SlowThreshold:             3 * time.Second, // Slow SQL threshold
					LogLevel:                  logger.Warn,     // Log level
					IgnoreRecordNotFoundError: true,            // Ignore ErrRecordNotFound error for logger
					Colorful:                  true,            // Disable color
				},
			),
		})
		if err != nil {
			fmt.Printf("Failed to connect to the database (attempt %d/%d): %v\n", attempt, maxRetries, err)
			if attempt < maxRetries {
				time.Sleep(retryDelay)
			} else {
				return nil, err
			}
		} else {
			// Successfully connected to the database
			break
		}
	}

	// Perform any necessary database migrations here
	db.AutoMigrate(
		&entities.Room{}, &entities.User{}, &entities.Transection{}, &entities.Booking{},
	)

	if conf.Debug == "true" {
		db = db.Debug()
	}
	return db, nil
}

func dsn(conf Config) gorm.Dialector {
	switch conf.DatabaseType {
	case "postgresql":
		return postgres.Open(getDSN(conf))
	default:
		return mysql.Open(getDSN(conf))
	}
}

func getDSN(conf Config) string {
	switch conf.DatabaseType {
	case "postgresql":
		return fmt.Sprintf(
			"host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
			conf.DatabaseHost,
			conf.DatabasePort,
			conf.DatabaseUsername,
			conf.DatabaseName,
			conf.DatabasePassword,
		)
	default:
		return fmt.Sprintf(
			"%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
			conf.DatabaseUsername,
			conf.DatabasePassword,
			conf.DatabaseHost,
			conf.DatabasePort,
			conf.DatabaseName,
		)
	}
}

// Close closes the database connection.x
func Close(db *gorm.DB) {
	if db != nil {
		sqlDB, err := db.DB()
		if err != nil {
			return
		}
		sqlDB.Close()
	}
}
